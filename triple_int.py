# -*- coding: utf-8 -*-
"""
Created on Wed Apr 26 22:38:57 2017

@author: Stanimir Kabaivanov
"""

import mcint
import random
from datetime import datetime

def integrand(p):
    x = p[0]
    y = p[1]
    z = p[2]
    return 8*x*y*z

def sampler():
    while True:
        z     = random.uniform(1.,2.)
        y     = random.uniform(0.,1.)
        x     = random.uniform(2.,3.)
        yield (x, y, z)

expected = 15.

for nmc in [1000, 10000, 100000, 1000000, 10000000, 100000000, 1000000000]:
    random.seed(datetime.now())
    result, error = mcint.integrate(integrand, sampler(), n=nmc)
    diff = abs(result - expected)

    print("Повторения n = ", nmc)
    print("Резултат = ", result, "изчислена грешка = ", error)
    print("Предварително ясен резултати = ", expected, " грешка = ", diff, " = ", 100.*diff/expected, "%")
    print(" ")